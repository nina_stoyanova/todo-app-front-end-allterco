## Known issues
- Color picker not always allows color pallett change 
  
  
## How to run the project
- npm install
- npm start
- open browser at localhost:3000


## Format and lint
- npm run format
- npm run lint


