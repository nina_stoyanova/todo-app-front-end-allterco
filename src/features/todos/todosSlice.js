import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

const initialState = {
  activeFilter: null,
  todos: [
    {
      id: '1',
      title: 'this is an example todo',
      completed: true,
      color: '#FFF'
    }
  ]
};

export const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    todosLoaded(state, action) {
      const data = action.payload.slice(0, 10).map((obj) => {
        const result = {
          id: uuidv4(),
          title: obj.title,
          completed: false,
          color: '#FFF'
        };
        return result;
      });
      state.todos = [...state.todos, ...data];
    },
    addTodo(state, action) {
      const result = {
        id: uuidv4(),
        title: action.payload,
        completed: false
      };
      state.todos.unshift(result);
    },
    deleteTodo(state, action) {
      const id = action.payload;
      const stayingTodos = state.todos.filter((todo) => todo.id !== id);
      state.todos = stayingTodos;
    },
    markAsCompleted(state, action) {
      const completedTodo = action.payload;
      const result = state.todos.map((todo) => {
        if (todo.id === completedTodo.id) {
          return {
            ...todo,
            completed: true
          };
        }
        return todo;
      });
      state.todos = result;
    },
    markAsNotCompleted(state, action) {
      const notCompletedTodo = action.payload;
      const result = state.todos.map((todo) => {
        if (todo.id === notCompletedTodo.id) {
          return {
            ...todo,
            completed: false
          };
        }
        return todo;
      });
      state.todos = result;
    },
    filterResolved(state) {
      state.activeFilter = 'resolved';
    },
    filterUnresolved(state) {
      state.activeFilter = 'unresolved';
    },
    filterByColor(state, action) {
      state.activeFilter = action.payload;
    },
    resetState(state) {
      state.activeFilter = null;
    },
    setPickedColor(state, action) {
      const { color, todo } = action.payload;
      const result = state.todos.map((t) => {
        if (t.id === todo.id) {
          return {
            ...t,
            color: color
          };
        }
        return t;
      });
      state.todos = result;
    },
    deleteAllTodos(state) {
      state.todos = [];
    },
    resolveAllTodos(state) {
      const result = state.todos.map((todo) => {
        return {
          ...todo,
          completed: true
        };
      });
      state.todos = result;
    },
    unresolveAllTodos(state) {
      const result = state.todos.map((todo) => {
        return {
          ...todo,
          completed: false
        };
      });
      state.todos = result;
    }
  }
});

export const fetchTodos = () => {
  return async (dispatch) => {
    try {
      const todos = await axios.get('https://jsonplaceholder.typicode.com/todos');
      dispatch(todosLoaded(todos.data));
    } catch (err) {
      console.log(err);
    }
  };
};

export const {
  todosLoaded,
  deleteTodo,
  addTodo,
  markAsCompleted,
  markAsNotCompleted,
  filterResolved,
  filterUnresolved,
  resetState,
  setPickedColor,
  deleteAllTodos,
  resolveAllTodos,
  unresolveAllTodos,
  filterByColor
} = todosSlice.actions;

export const select = {
  filteredTodos: (state) => {
    if (state.todos.activeFilter === 'resolved') {
      return state.todos.todos.filter((todo) => todo.completed === true);
    } else if (state.todos.activeFilter === 'unresolved') {
      return state.todos.todos.filter((todo) => todo.completed === false);
    } else if (state.todos.activeFilter !== null && state.todos.activeFilter.startsWith('#')) {
      return state.todos.todos.filter((todo) => todo.color === state.todos.activeFilter);
    } else {
      return state.todos.todos;
    }
  },
  activeFilter: (state) => state.todos.activeFilter,
  uniqueColors: (state) => {
    const colors = state.todos.todos.map((todo) => {
      return todo.color;
    });
    const result = new Set(colors);
    return [...result];
  }
};

export default todosSlice.reducer;
