import { Button, Modal, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { filterResolved, filterUnresolved, resetState, select } from '../../todosSlice';
import ColorFilters from '../colorFilters/ColorFilters';
import './filterTodos.css';

export default function FilterTodos() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useDispatch();
  const activeFilter = useSelector(select.activeFilter);

  const filterByResolved = () => {
    dispatch(filterResolved());
  };

  const filterByUnreslved = () => {
    dispatch(filterUnresolved());
  };

  const clearFilters = () => {
    dispatch(resetState());
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'white',
    border: 'none',
    boxShadow: 24,
    p: 4
  };

  return (
    <div className="filter-buttons">
      <Button
        style={{ margin: 10 }}
        variant={activeFilter === 'resolved' ? 'contained' : 'text'}
        onClick={() => filterByResolved()}
      >
        resolved
      </Button>
      <Button
        style={{ margin: 10 }}
        variant={activeFilter === 'unresolved' ? 'contained' : 'text'}
        onClick={() => filterByUnreslved()}
      >
        unresolved
      </Button>
      <Button
        style={{ margin: 10 }}
        variant={activeFilter && activeFilter.startsWith('#') ? 'contained' : 'text'}
        onClick={() => {
          handleOpenModal();
        }}
      >
        color
      </Button>
      <Button style={{ margin: 10 }} onClick={() => clearFilters()}>
        Clear filters
      </Button>
      <Modal open={isModalOpen} onClose={handleCloseModal}>
        <Box sx={style}>
          <Typography style={{ margin: 10 }} id="modal-modal-title" variant="h6" component="h2">
            Select color filter
          </Typography>
          <ColorFilters />
        </Box>
      </Modal>
    </div>
  );
}
