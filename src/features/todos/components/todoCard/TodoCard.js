import React, { useState } from 'react';
import ClearIcon from '@mui/icons-material/Clear';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import { useDispatch } from 'react-redux';
import { deleteTodo, markAsCompleted, markAsNotCompleted, setPickedColor } from '../../todosSlice';
import './TodoCard.css';
import ColorTag from '../colorTag/ColorTag';
import { Button, Modal, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { ColorBox } from 'material-ui-color';
import { makeStyles } from '@material-ui/core';

export default function TodoCard({ todo }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [color, setColor] = useState(todo.color);
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    root: {
      border: 'none'
    }
  });
  const classes = useStyles();

  const removeTodo = (id) => {
    dispatch(deleteTodo(id));
  };

  const handleCompleted = (todo) => {
    dispatch(markAsCompleted(todo));
  };

  const handleNotCompleted = (todo) => {
    dispatch(markAsNotCompleted(todo));
  };

  const handleOpenModal = () => setIsModalOpen(true);
  const handleCloseModal = () => {
    setColor(todo.color);
    setIsModalOpen(false);
  };

  const handlePickedColor = (color) => {
    setColor(color);
  };

  const handleSelectedColor = () => {
    dispatch(setPickedColor({ color: color, todo: todo }));
    setIsModalOpen(false);
  };

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'white',
    border: 'none',
    boxShadow: 24,
    p: 4
  };

  return (
    <React.Fragment>
      <div className="todo-card">
        <p className="todo-title">{todo.title}</p>
        <div className="todo-icons">
          {todo.completed ? (
            <CheckBoxIcon style={{ cursor: 'pointer' }} onClick={() => handleNotCompleted(todo)} />
          ) : (
            <CheckBoxOutlineBlankIcon
              style={{ cursor: 'pointer' }}
              onClick={() => handleCompleted(todo)}
            />
          )}
          <ClearIcon style={{ cursor: 'pointer' }} onClick={() => removeTodo(todo.id)} />
        </div>
        <div className="todo-color">
          <ColorTag color={todo.color} onClick={() => handleOpenModal()} />
        </div>
      </div>
      <Modal open={isModalOpen} onClose={handleCloseModal}>
        <Box sx={style}>
          <Typography style={{ margin: 10 }} id="modal-modal-title" variant="h6" component="h2">
            Select color
          </Typography>
          <ColorBox
            disableAlpha
            classes={classes}
            value={color}
            onChange={(color) => {
              handlePickedColor(color.css.backgroundColor);
            }}
          />
          <Button
            sx={{
              mt: 4
            }}
            variant="contained"
            onClick={() => {
              handleSelectedColor();
            }}
          >
            Pick
          </Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}
