import React from 'react';
import { TextField } from '@mui/material';
import { addTodo } from '../../todosSlice';
import { useDispatch } from 'react-redux';

export default function AddTodos() {
  const [todo, setTodo] = React.useState('');
  const dispatch = useDispatch();

  const handleChange = (e) => {
    setTodo(e.target.value);
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      dispatch(addTodo(todo));
    }
  };

  return (
    <React.Fragment>
      <TextField
        placeholder="Enter a todo"
        id="outlined-name"
        value={todo}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
      />
    </React.Fragment>
  );
}
