import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { filterByColor, select } from '../../todosSlice';
import ColorTag from '../colorTag/ColorTag';
import './ColorFilters.css';

export default function ColorFilters() {
  const colors = useSelector(select.uniqueColors);
  const dispatch = useDispatch();

  const handleColorFilter = (color) => {
    dispatch(filterByColor(color));
  };

  return (
    <div className="color-filters">
      {colors.map((color, i) => {
        return <ColorTag key={i} color={color} onClick={(color) => handleColorFilter(color)} />;
      })}
    </div>
  );
}
