import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchTodos, select } from '../../todosSlice';
import { useEffect } from 'react';
import FilterTodos from '../filterTodos/FilterTodos';
import TodoCard from '../todoCard/TodoCard';

function TodosList() {
  const dispatch = useDispatch();
  const todos = useSelector(select.filteredTodos);

  useEffect(() => {
    dispatch(fetchTodos());
  }, [dispatch]);

  return (
    <div className="todo-container">
      <FilterTodos />
      {todos &&
        todos.map((todo) => {
          return <TodoCard key={todo.id} todo={todo} />;
        })}
    </div>
  );
}

export default TodosList;
