import React from 'react';

export default function ColorTag(props) {
  return (
    <div
      onClick={() => props.onClick(props.color)}
      style={{
        width: 25,
        height: 25,
        backgroundColor: props.color,
        borderRadius: '50%',
        cursor: 'pointer',
        marginRight: 5,
        border: '1px solid black'
      }}
    ></div>
  );
}
