import { Grid, Paper } from '@mui/material';
import { Container } from '@mui/system';
import React from 'react';
import Layout from '../components/Layout';
import AddTodos from '../features/todos/components/addTodos/AddTodos';
import TodosList from '../features/todos/components/todoList/TodosList';

export default function TodosPage() {
  return (
    <Layout>
      <Container maxWidth="sm" sx={{ mt: 4, mb: 4 }}>
        <Grid container spacing={10}>
          <Grid item xs={12} md={12} lg={12}>
            <Paper
              sx={{
                p: 2,
                display: 'flex',
                flexDirection: 'column',
                height: 100
              }}
            >
              <AddTodos />
            </Paper>
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
              <TodosList />
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
}
