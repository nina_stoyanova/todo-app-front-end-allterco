import { List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import React from 'react';
import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone';
import DoneAllTwoToneIcon from '@mui/icons-material/DoneAllTwoTone';
import PendingActionsTwoToneIcon from '@mui/icons-material/PendingActionsTwoTone';
import { useDispatch } from 'react-redux';
import { deleteAllTodos, resolveAllTodos, unresolveAllTodos } from '../features/todos/todosSlice';

export default function SideMenu() {
  const dispatch = useDispatch();

  const handleAllDelete = () => {
    dispatch(deleteAllTodos());
  };

  const handleAllResolved = () => {
    dispatch(resolveAllTodos());
  };
  const hanldeAllUnresolved = () => {
    dispatch(unresolveAllTodos());
  };

  return (
    <List>
      <ListItem key={'Mark all as resolved'} disablePadding sx={{ display: 'block' }}>
        <ListItemButton
          sx={{
            minHeight: 48,
            justifyContent: open ? 'initial' : 'center',
            px: 2.5
          }}
        >
          <ListItemIcon
            sx={{
              minWidth: 0,
              mr: open ? 3 : 'auto',
              justifyContent: 'center'
            }}
          >
            <DoneAllTwoToneIcon onClick={handleAllResolved} />
          </ListItemIcon>
          <ListItemText primary={'Mark all as resolved'} sx={{ opacity: open ? 1 : 0 }} />
        </ListItemButton>
      </ListItem>
      <ListItem key={'Mark all as unresolved'} disablePadding sx={{ display: 'block' }}>
        <ListItemButton
          sx={{
            minHeight: 48,
            justifyContent: open ? 'initial' : 'center',
            px: 2.5
          }}
        >
          <ListItemIcon
            sx={{
              minWidth: 0,
              mr: open ? 3 : 'auto',
              justifyContent: 'center'
            }}
          >
            <PendingActionsTwoToneIcon onClick={hanldeAllUnresolved} />
          </ListItemIcon>
          <ListItemText primary={'Mark all as unresolved'} sx={{ opacity: open ? 1 : 0 }} />
        </ListItemButton>
      </ListItem>
      <ListItem key={'Delete all'} disablePadding sx={{ display: 'block' }}>
        <ListItemButton
          sx={{
            minHeight: 48,
            justifyContent: open ? 'initial' : 'center',
            px: 2.5
          }}
        >
          <ListItemIcon
            sx={{
              minWidth: 0,
              mr: open ? 3 : 'auto',
              justifyContent: 'center'
            }}
          >
            <DeleteTwoToneIcon onClick={handleAllDelete} />
          </ListItemIcon>
          <ListItemText primary={'Delete all'} sx={{ opacity: open ? 1 : 0 }} />
        </ListItemButton>
      </ListItem>
    </List>
  );
}
