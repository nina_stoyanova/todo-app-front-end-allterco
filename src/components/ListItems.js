import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DoneIcon from '@mui/icons-material/Done';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

export const mainListItems = (
  <React.Fragment>
    <ListItemButton>
      <ListItemIcon>
        <DoneIcon />
      </ListItemIcon>
      <ListItemText primary="Mark all as resolved" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <QuestionMarkIcon />
      </ListItemIcon>
      <ListItemText primary="Mark all as unresolved" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <HighlightOffIcon />
      </ListItemIcon>
      <ListItemText primary="Remove all" />
    </ListItemButton>
  </React.Fragment>
);
